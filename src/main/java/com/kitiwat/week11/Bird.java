package com.kitiwat.week11;

public class Bird extends Animal implements Flyable {

    public Bird(String name) {
        super(name,2);
    }

    @Override
    public void fly() {
        System.out.println(this + " fly. ");
    }

    @Override
    public void landing() {
        System.out.println(this + " landing. ");
    }
    @Override
    public void takeoff() {
        System.out.println(this + " takeoff. ");
    }
    @Override
    public void eat() {
        System.out.println(this + " eat. ");
    }
    @Override
    public void sleep() {
        System.out.println(this + " sleep. ");
    }
    @Override
    public String toString() {
        return "Bird(" + this.getName() + ")";
    }
    
}
