package com.kitiwat.week11;

public class Submarine extends Vehicle implements Swimable {

    public Submarine(String name, String engineName) {
        super(name, engineName);
        
    }
    @Override
    public void swim() {
        System.out.println(this + " swim. ");
    }
    @Override
    public String toString() {
        return "Submarine(" + this.getName() + ")";
    }
    
}
