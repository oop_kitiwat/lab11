package com.kitiwat.week11;



/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        Bat bat1 = new Bat("BatMoew");
        bat1.eat();
        bat1.sleep();
        bat1.takeoff();
        bat1.fly();
        bat1.landing();
        System.out.println("==============================");
        Fish f1 = new Fish("Playood");
        f1.eat();
        f1.swim();
        f1.sleep();
        System.out.println("==============================");
        Plane pl1 = new Plane("Nok Kao Air", "Nitro");
        pl1.takeoff();
        pl1.fly();
        pl1.landing();
        System.out.println("==============================");
        Human h1 = new Human("Akashi");
        h1.eat();
        h1.sleep();
        h1.walk();
        h1.run();
        System.out.println("==============================");
        Snake sn1 = new Snake("KumKaew");
        sn1.crawl();
        sn1.eat();
        sn1.swim();
        sn1.sleep();
        System.out.println("==============================");
        Crocodile coc1 = new Crocodile("Chalawan");
        coc1.crawl();
        coc1.swim();
        coc1.eat();
        coc1.sleep();
        System.out.println("==============================");
        Submarine sub1 = new Submarine("PaSeeGoo", "PaSeeGooAgain");
        sub1.swim();
        System.out.println("==============================");
        Bird bird = new Bird("ThongChai");
        bird.eat();
        bird.takeoff();
        bird.fly();
        bird.landing();
        bird.sleep();
        System.out.println("==============================");
        Cat cat = new Cat("Panda",4);
        cat.eat();
        cat.walk();
        cat.run();
        cat.sleep();
        System.out.println("==============================");
        Dog dog = new Dog("Rod Thung");
        dog.eat();
        dog.walk();
        dog.run();
        dog.sleep();
        System.out.println("==============================");
        
        Flyable[] flyableObject = {bat1,pl1,bird};
        for (int i=0; i < flyableObject.length; i++) {
            flyableObject[i].takeoff();
            flyableObject[i].fly();
            flyableObject[i].landing();
        }
        System.out.println("==============================");


        Swimable[] swimableObject = {f1,coc1,sub1};
        for (int i=0; i < swimableObject.length; i++) {
            swimableObject[i].swim();
        }
        System.out.println("==============================");
        Crawlable[] crawlableObject = {sn1,coc1};
        for (int i=0; i < crawlableObject.length; i++){
            crawlableObject[i].crawl();
        }
        System.out.println("==============================");
        Walkable[] walkablesObject = {h1,cat,dog};
        for (int i=0; i < walkablesObject.length; i++){
            walkablesObject[i].walk();
            walkablesObject[i].run();
        }












    }
}

