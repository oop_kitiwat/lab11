package com.kitiwat.week11;



public abstract class Animal { 
    private String name;
    private int numberOfLeg;

    public Animal(String name , int numberOfLeg) {
        this.name = name;
        this.numberOfLeg = numberOfLeg;
    }
    public String getName() { 
        return name;
    }
    public int getnumberOfLeg() {
        return numberOfLeg;
    }
    public void setName(String name) {
        this.name = name;
    } 
    public void setnumberOfLeg(int numberOfLeg) {
        this.numberOfLeg = numberOfLeg;
    }

    @Override
    public String toString() {
        return "Animal (" + name + ") has " + numberOfLeg + " legs";
    }

    public abstract void eat();
    public abstract void sleep();
    


    
}
